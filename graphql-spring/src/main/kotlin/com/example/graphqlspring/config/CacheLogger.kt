package com.example.graphqlspring.config

import org.ehcache.event.CacheEvent
import org.ehcache.event.CacheEventListener
import org.slf4j.LoggerFactory
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

class CacheLogger : CacheEventListener<Any, Any> {

    companion object {
        private val LOGGER = LoggerFactory.getLogger(CacheLogger::class.java)
    }

    override fun onEvent(cacheEvent: CacheEvent<out Any, out Any>?) {
        LOGGER.info("Key: {} | Event type: {} | Old value: {} | New value: {}", cacheEvent?.key, cacheEvent?.type,
                cacheEvent?.oldValue, cacheEvent?.newValue)
    }

}
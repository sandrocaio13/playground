package com.example.graphqlspring.model

import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.entity.NewEmployee
import com.example.graphqlspring.model.repository.EmployeeRepository
import com.example.graphqlspring.model.repository.NewEmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.cache.annotation.Cacheable

@Component
class EmployeeImpl @Autowired constructor(private val employeeRepository: EmployeeRepository,
                                          private val newEmployeeRepository: NewEmployeeRepository) {

    @Cacheable("employee")
    fun findAll(): MutableIterable<NewEmployee> = newEmployeeRepository.findAll()

}
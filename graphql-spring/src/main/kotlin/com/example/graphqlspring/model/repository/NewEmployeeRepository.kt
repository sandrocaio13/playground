package com.example.graphqlspring.model.repository

import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.entity.NewEmployee
import org.springframework.data.repository.PagingAndSortingRepository
import javax.persistence.Cacheable

interface NewEmployeeRepository: PagingAndSortingRepository<NewEmployee, String> {

    fun findById(employeeId: Long): NewEmployee?

}
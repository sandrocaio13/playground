package com.example.graphqlspring.model.entity

import java.util.*
import javax.persistence.*

@Entity(name = "empregado")
data class NewEmployee (

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @Column(name = "idade", nullable = false)
        var age: Int? = null,

        @Column(name = "nascimento", nullable = false)
        var birthDate: Date? = null,

        @Column(name = "nome", nullable = false)
        var name: String? = null
)
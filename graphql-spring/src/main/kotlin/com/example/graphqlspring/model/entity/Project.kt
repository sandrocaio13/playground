package com.example.graphqlspring.model.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity(name = "projeto")
data class Project(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @Column(name = "codigo_servico", nullable = false)
        var serviceCode: String? = null,

        @Column(name = "prazo", nullable = false)
        var deadline: Date? = null,

        @Column(name = "nome", nullable = false)
        var projectName: String? = null,

        @Column(name = "responsavel", nullable = false)
        var responsible: String? = null,

        @ManyToOne
        @JoinColumn(name = "empregado_id", nullable = false, updatable = false)
        var employee: Employee? = null
)
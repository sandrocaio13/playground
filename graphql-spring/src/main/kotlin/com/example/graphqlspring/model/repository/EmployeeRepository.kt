package com.example.graphqlspring.model.repository

import com.example.graphqlspring.model.entity.Employee
import org.springframework.data.repository.PagingAndSortingRepository
import javax.persistence.Cacheable

interface EmployeeRepository: PagingAndSortingRepository<Employee, String> {

    fun findById(employeeId: Long): Employee?

    fun findByProjects(projectId: Long): Employee?

}
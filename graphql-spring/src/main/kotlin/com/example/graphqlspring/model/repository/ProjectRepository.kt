package com.example.graphqlspring.model.repository

import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.entity.Project
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

interface ProjectRepository: PagingAndSortingRepository<Project, String> {

    fun findById(projectId: Long): Project

    fun findByEmployee(employee: Employee): Project

}
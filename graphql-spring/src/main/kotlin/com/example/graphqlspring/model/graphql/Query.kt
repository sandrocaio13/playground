package com.example.graphqlspring.model.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.repository.EmployeeRepository
import com.example.graphqlspring.model.repository.ProjectRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class Query(private val employeeRepository: EmployeeRepository,
            private val projectRepository: ProjectRepository) : GraphQLQueryResolver, GraphQLMutationResolver {

    fun getEmployees() = employeeRepository.findAll()

    fun countEmployees() = employeeRepository.count()

    fun getEmployeeById(employeeId: Long) = employeeRepository.findById(employeeId)

    fun getAllProjects() = projectRepository.findAll()

    fun countProjects() = projectRepository.count()

    fun getProjectById(projectId: Long) = projectRepository.findById(projectId)

    fun createEmployee(name: String, age: Int, birthDate: Date): Employee {
        val employee = Employee(name = name, age = age, birthDate = birthDate)
        employeeRepository.save(employee)
        return employee
    }

}
package com.example.graphqlspring

import com.example.graphqlspring.model.repository.EmployeeRepository
import com.example.graphqlspring.model.graphql.Query
import com.example.graphqlspring.model.repository.ProjectRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean

@SpringBootApplication
@EntityScan
class GraphqlSpringApplication

@Bean
fun query(employeeRepository: EmployeeRepository, projectRepository: ProjectRepository): Query {
    return Query(employeeRepository, projectRepository)
}

fun main(args: Array<String>) {
	runApplication<GraphqlSpringApplication>(*args)
}
package com.example.graphqlspring.controller

import com.example.graphqlspring.services.RadiusService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["areaa/Circle/rest/v1"])
class AreaCircleController @Autowired constructor(private val radiusService: RadiusService) : BaseController() {

    @GetMapping(value = ["/"])
    fun areaOfCircle(@RequestParam radius: Double): ResponseEntity<Double> {
        return ResponseEntity.ok(radiusService.areaOfCircle(radius))
    }

}
package com.example.graphqlspring.controller

import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.entity.NewEmployee
import com.example.graphqlspring.model.entity.Project
import com.example.graphqlspring.services.GraphqlService
import graphql.GraphQL
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/rest/v1"])
@Validated
class GraphqlController @Autowired constructor(private val graphqlService: GraphqlService): BaseController() {

    companion object {
        val LOGGER = LoggerFactory.getLogger(GraphqlController::class.java)
    }

    @GetMapping(value = ["/getEmployee/{userId}"])
    fun getEmployee(@PathVariable("userId") userId: Long): ResponseEntity<Employee> {
        val response = graphqlService.getProjectUser(userId)
        LOGGER.info("Rest controller getEmployee with user id: {}", userId)
        return ResponseEntity.ok(response!!)
    }

    @GetMapping(value = ["/getProject/{projectId}"])
    fun getProject(@PathVariable("projectId") projectId: Long): ResponseEntity<Project> {
        val response = graphqlService.getProject(projectId)
        LOGGER.info("Rest controller getProject with user id: {}", projectId)
        return ResponseEntity.ok(response)
    }

    @GetMapping(value = ["/getEmployee/withCache"])
    fun getProjectsWithCache(): MutableIterable<NewEmployee> {
        LOGGER.info("Return with cache")
        return graphqlService.getAllProjectsWithCache()
    }

    @GetMapping(value = ["/getEmployee/withoutCache"])
    fun getProjectsWithoutCache(): MutableIterable<NewEmployee> {
        LOGGER.info("Return without cache")
        return graphqlService.getAllProjects()
    }

}
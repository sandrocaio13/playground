package com.example.graphqlspring.controller

import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.InitBinder
import java.beans.PropertyEditorSupport
import java.util.*

open class BaseController {

    @InitBinder
    fun initBinder(binder: WebDataBinder) {
        binder.registerCustomEditor(Date::class.java, object : PropertyEditorSupport() {
            override fun setAsText(timeStampAsText: String) {
                val timestamp = timeStampAsText.toLong()
                value = Date(timestamp)
            }
        })
    }
}
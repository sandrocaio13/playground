package com.example.graphqlspring.services

import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class RadiusService {

    companion object {
        val LOGGER = LoggerFactory.getLogger(RadiusService::class.java)
    }

    @Cacheable(value = ["areaOfCircleCache"], key = "#radius", condition = "#radius > 5")
    fun areaOfCircle(radius: Double): Double {
        LOGGER.info("calculate the area of circle with radius {}", radius)
        return Math.PI * Math.pow(radius, 2.0)
    }

}
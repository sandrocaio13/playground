package com.example.graphqlspring.services

import com.example.graphqlspring.model.EmployeeImpl
import com.example.graphqlspring.model.entity.Employee
import com.example.graphqlspring.model.entity.NewEmployee
import com.example.graphqlspring.model.entity.Project
import com.example.graphqlspring.model.graphql.Query
import com.example.graphqlspring.model.repository.EmployeeRepository
import com.example.graphqlspring.model.repository.NewEmployeeRepository
import com.example.graphqlspring.model.repository.ProjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class GraphqlService @Autowired constructor(
        private val employeeRepository: EmployeeRepository,
        private val projectRepository: ProjectRepository,
        private val newEmployeeRepository: NewEmployeeRepository,
        private val employeeImpl: EmployeeImpl,
        private val query: Query) {

    @Transactional
    fun getProjectUser(employeeId: Long): Employee? {
        employeeId.let {
            return employeeRepository.findById(it)
        }
    }

    @Transactional
    fun getProject(projectId: Long): Project {
        projectId.let {
            val project = projectRepository.findById(projectId)
            return Project(
                    id = project.id,
                    projectName = project.projectName,
                    responsible = project.responsible,
                    deadline = project.deadline
            )
        }
    }

    @Transactional
    fun getAllProjectsWithCache(): MutableIterable<NewEmployee> {
        return employeeImpl.findAll()
    }

    @Transactional
    fun getAllProjects(): MutableIterable<NewEmployee> {
        return newEmployeeRepository.findAll()
    }

}